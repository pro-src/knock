grongor/knock
=============

Simple utility for port knocking written in python3.

If you find it useful but you think it lacks some functionality please let me know by creating an issue. Thank you!

Basic usage
-----------

`knock your.server.com 1234 8521 4785`

Usage with [knockd](http://www.zeroflux.org/projects/knock)
---------------
```shell
knock your.server.com --in-file ./sequence-file`
knock --generate > ./sequence-file
knock --out-file ./sequence-file
```

Options
-------

`knock --help` will tell you everything you need. Here is the output to save you a few seconds:

```
usage: knock [-h] [-t TIMEOUT] [-d DELAY] [-u] [-i IN_FILE | -o OUT_FILE | -g]
             [--count COUNT] [--lines LINES]
             [host] [port[:protocol] [port[:protocol] ...]]

Simple port-knocking client written in python3. See more at
https://github.com/pro-src/knock

positional arguments:
  host                  Hostname or IP address of the host to knock on.
                        Supports IPv6.
  port[:protocol]       Port(s) to knock on, protocol (tcp, udp) is optional.

optional arguments:
  -h, --help            show this help message and exit
  -t TIMEOUT, --timeout TIMEOUT
                        How many milliseconds to wait on hanging connection.
                        Default is 200 ms.
  -d DELAY, --delay DELAY
                        How many milliseconds to wait between each knock.
                        Default is 200 ms.
  -u, --udp             Use UDP instead of TCP by default.
  -i IN_FILE, --in-file IN_FILE
                        Read port sequences from a knockd compatible file. The
                        default is /home/dwayne/.knockd-sequences if ports are
                        not specified.
  -o OUT_FILE, --out-file OUT_FILE
                        Generate a knockd compatible file of port sequences.
  -g, --generate        Print knockd compatible sequences to stdout.
  --count COUNT         How many random ports to generate for a sequence.
                        Default is 10.
  --lines LINES         Line count, how many random sequences to generate.
                        Default is 1000.
```
